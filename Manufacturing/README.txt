===============================================================================
Organization : UBC ORBIT / SFU SAT
Project      : ALEA
Subteam      : ADCS
PCB          : ADCS_Daughterboard
Revision     : 1.0.0
===============================================================================
 
-------------------------------------------------------------------------------
General
-------------------------------------------------------------------------------
 
Width                 : 26.3mm
Height                : 34.1mm
# Layers              : 2
Hole types            : PTH + NPTH
Component types       : Surface-Mount + Through-Hole
Components on top     : Yes
Components on bottom  : Yes
Silkscreen on top     : Yes
Silkscreen on bottom  : Yes
 
-------------------------------------------------------------------------------
Included Files
-------------------------------------------------------------------------------
 
Board Profile         : ADCS_Daughterboard-Edge_Cuts.gbr
 
Schematic             : ADCS_Daughterboard_Schematic.pdf
 
Bill of Materials     : ADCS_Daughterboard_BOM.xlsx
 
Gerber Files:
 
    Filename                           Polarity
    -----------------------------------------------
    ADCS_Daughterboard-F_SilkS.gbr       Positive
    ADCS_Daughterboard-F_Cu.gbr          Positive
    ADCS_Daughterboard-F_Paste.gbr       Positive
    ADCS_Daughterboard-F_Mask.gbr        Negative
    ADCS_Daughterboard-B_Mask.gbr        Negative
    ADCS_Daughterboard-B_Paste.gbr       Positive
    ADCS_Daughterboard-B_Cu.gbr          Positive
    ADCS_Daughterboard-B_SilkS.gbr	 Postivee
 
Drill Files:
 
    Filename                    Description
    -------------------------------------------------------
    ADCS_Daughterboard-PTH-drl_map.gbr    Plated through holes (Gerber)
    ADCS_Daughterboard-PTH.drl	          Plated through holes (Drill)
    ADCS_Daughterboard-NPTH-drl_map.gbr   Non-Plated through holes (Gerber)
    ADCS_Daughterboard-NPTH.drl           Non-Plated through holes (Drill)
 
-------------------------------------------------------------------------------
Board Stackup
-------------------------------------------------------------------------------
 
See ADCS_Daughterboard_Stackup.xlsx
 
-------------------------------------------------------------------------------
Appearance
-------------------------------------------------------------------------------
 
Soldermask Color : Red
Silkscreen Color : White
Surface Finish   : HASL with lead