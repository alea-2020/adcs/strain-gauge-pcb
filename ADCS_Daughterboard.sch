EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 8300 4150 0    50   ~ 0
1: SDA\n2: SCL\n3: V_in\n4: GND
Text Notes 6550 3100 0    50   ~ 0
ADR1&2 are connected to V_in, ground, or floating depending on\n I2C bus requirements. OBC needs to tell us what they want the \naddress to be. Logic table is on page 21 of datasheet\n\nCurrently Andy has no preference for address so we will ground\nboth pins for an address of 000
Text Notes 5050 5450 0    50   ~ 0
Pull up values are dependant on i2c bus speed.\nSee page 12 of datasheet\n\nAndy says we are using 100khz so 5kOhm was chosen.\n\nUpdate: I read that only one set of pulls ups are needed \nper bus, need to check with Andy to see if OBC already \nhas pulls ups\n\nRemoved pull-ups
Wire Wire Line
	4150 4600 4150 4500
$Comp
L Device:C C2
U 1 1 5EE638BB
P 4300 4600
F 0 "C2" V 4550 4600 50  0000 C CNN
F 1 "10uF" V 4450 4600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4338 4450 50  0001 C CNN
F 3 "~" H 4300 4600 50  0001 C CNN
	1    4300 4600
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 5EE6302F
P 4300 4400
F 0 "C1" V 4048 4400 50  0000 C CNN
F 1 "0.1uF" V 4139 4400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4338 4250 50  0001 C CNN
F 3 "~" H 4300 4400 50  0001 C CNN
	1    4300 4400
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Female J2
U 1 1 5EEA6911
P 7500 4000
F 0 "J2" H 7528 3976 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7528 3885 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7500 4000 50  0001 C CNN
F 3 "~" H 7500 4000 50  0001 C CNN
	1    7500 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5EEA6CE5
P 7300 4200
F 0 "#PWR0101" H 7300 3950 50  0001 C CNN
F 1 "GND" H 7305 4027 50  0000 C CNN
F 2 "" H 7300 4200 50  0001 C CNN
F 3 "" H 7300 4200 50  0001 C CNN
	1    7300 4200
	1    0    0    -1  
$EndComp
Text Notes 1950 5150 0    50   ~ 0
datasheet calls for C2 to be tantalum. Could find no reason it had to\nbe other than energy density reasons, therefore since this is a\nspace application we will just use ceramic
Text Notes 7500 4500 0    50   ~ 0
No digital ground plane required. SG is analog, \nVSS is analog, bypass caps are analog
Text Label 7150 4550 0    50   ~ 0
V_in
$Comp
L power:GND #PWR0103
U 1 1 5F1902B6
P 5100 3500
F 0 "#PWR0103" H 5100 3250 50  0001 C CNN
F 1 "GND" V 5105 3372 50  0000 R CNN
F 2 "" H 5100 3500 50  0001 C CNN
F 3 "" H 5100 3500 50  0001 C CNN
	1    5100 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	4450 4400 4450 4600
Text GLabel 4450 4500 2    50   Input ~ 0
V_in
$Comp
L power:GND #PWR0104
U 1 1 5F197DEE
P 4150 4500
F 0 "#PWR0104" H 4150 4250 50  0001 C CNN
F 1 "GND" V 4155 4372 50  0000 R CNN
F 2 "" H 4150 4500 50  0001 C CNN
F 3 "" H 4150 4500 50  0001 C CNN
	1    4150 4500
	0    1    1    0   
$EndComp
Connection ~ 4150 4500
Wire Wire Line
	4150 4500 4150 4400
Text GLabel 7300 4100 0    50   Input ~ 0
V_in
Text GLabel 4300 2650 2    50   Input ~ 0
V_in
Text GLabel 4300 3050 2    50   Input ~ 0
V_in
Text GLabel 4300 3450 2    50   Input ~ 0
V_in
Text GLabel 4300 3850 2    50   Input ~ 0
V_in
$Comp
L power:GND #PWR0105
U 1 1 5F1A5D7D
P 4300 2750
F 0 "#PWR0105" H 4300 2500 50  0001 C CNN
F 1 "GND" V 4305 2622 50  0000 R CNN
F 2 "" H 4300 2750 50  0001 C CNN
F 3 "" H 4300 2750 50  0001 C CNN
	1    4300 2750
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5F1A5A0D
P 4300 3150
F 0 "#PWR0106" H 4300 2900 50  0001 C CNN
F 1 "GND" V 4305 3022 50  0000 R CNN
F 2 "" H 4300 3150 50  0001 C CNN
F 3 "" H 4300 3150 50  0001 C CNN
	1    4300 3150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5F1A563F
P 4300 3550
F 0 "#PWR0107" H 4300 3300 50  0001 C CNN
F 1 "GND" V 4305 3422 50  0000 R CNN
F 2 "" H 4300 3550 50  0001 C CNN
F 3 "" H 4300 3550 50  0001 C CNN
	1    4300 3550
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5F1A4B6B
P 4300 3950
F 0 "#PWR0108" H 4300 3700 50  0001 C CNN
F 1 "GND" V 4305 3822 50  0000 R CNN
F 2 "" H 4300 3950 50  0001 C CNN
F 3 "" H 4300 3950 50  0001 C CNN
	1    4300 3950
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x16_Female J1
U 1 1 5F180006
P 4100 3250
F 0 "J1" H 3992 2225 50  0000 C CNN
F 1 "Conn_01x16_Female" H 3992 2316 50  0000 C CNN
F 2 "strain-gauge-pcb:3M_16POS_Conn" H 4100 3250 50  0001 C CNN
F 3 "~" H 4100 3250 50  0001 C CNN
	1    4100 3250
	-1   0    0    1   
$EndComp
Text Notes 3350 3950 0    50   ~ 0
16 = SG3_A\n15 = SG3_C\n14 = V_in\n13 = GND\n\n12 = SG4_C\n11 = SG4_A\n10 = V_in\n9 =GND\n\n8  = SG1_C\n7 = SG1_A\n6 =V_in\n5 =GND\n\n4 = SG2_C\n3 = SG2_A\n2 = V_In\n1 = GND
$Comp
L SamacSys_Parts:MCP3428-E_SL IC1
U 1 1 5EE56E25
P 5100 3100
F 0 "IC1" H 5650 3365 50  0000 C CNN
F 1 "MCP3428-E_SL" H 5650 3274 50  0000 C CNN
F 2 "SamacSys_Parts2:SOIC127P600X175-14N" H 6050 3200 50  0001 L CNN
F 3 "" H 6050 3100 50  0001 L CNN
F 4 "16-bit delta-sigma ADC, quad channel" H 6050 3000 50  0001 L CNN "Description"
F 5 "1.75" H 6050 2900 50  0001 L CNN "Height"
F 6 "579-MCP3428E/SL" H 6050 2800 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Microchip-Technology/MCP3428-E-SL?qs=icxrp76fIjD%2FAq4p7ExZpg%3D%3D" H 6050 2700 50  0001 L CNN "Mouser Price/Stock"
F 8 "Microchip" H 6050 2600 50  0001 L CNN "Manufacturer_Name"
F 9 "MCP3428-E/SL" H 6050 2500 50  0001 L CNN "Manufacturer_Part_Number"
	1    5100 3100
	1    0    0    -1  
$EndComp
Text GLabel 5100 3200 0    50   Input ~ 0
CH1-
Text GLabel 5100 3400 0    50   Input ~ 0
CH2-
Text GLabel 6200 3300 2    50   Input ~ 0
CH3-
Text GLabel 6200 3100 2    50   Input ~ 0
CH4-
Text GLabel 5100 3100 0    50   Input ~ 0
CH1+
Text GLabel 5100 3300 0    50   Input ~ 0
CH2+
Text GLabel 6200 3400 2    50   Input ~ 0
CH3+
Text GLabel 6200 3200 2    50   Input ~ 0
CH4+
Text GLabel 4300 3250 2    50   Input ~ 0
CH1-
Text GLabel 4300 3350 2    50   Input ~ 0
CH1+
Text Notes 5300 2200 0    50   ~ 0
SGn_C=CHn-\nSGn_A=CHn+
Text GLabel 4300 2850 2    50   Input ~ 0
CH4-
Text GLabel 4300 2950 2    50   Input ~ 0
CH4+
Text GLabel 4300 2550 2    50   Input ~ 0
CH3-
Text GLabel 4300 2450 2    50   Input ~ 0
CH3+
Text GLabel 4300 3750 2    50   Input ~ 0
CH2+
Text GLabel 4300 3650 2    50   Input ~ 0
CH2-
Text GLabel 5100 3600 0    50   Input ~ 0
V_in
Text GLabel 6200 3700 2    50   Input ~ 0
SCL
Text GLabel 7300 4000 0    50   Input ~ 0
SCL
Text GLabel 7300 3900 0    50   Input ~ 0
SDA
Text GLabel 5100 3700 0    50   Input ~ 0
SDA
$Comp
L power:GND #PWR0102
U 1 1 5F4D9471
P 6550 3700
F 0 "#PWR0102" H 6550 3450 50  0001 C CNN
F 1 "GND" H 6555 3527 50  0000 C CNN
F 2 "" H 6550 3700 50  0001 C CNN
F 3 "" H 6550 3700 50  0001 C CNN
	1    6550 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5F4DA1DE
P 6550 3400
F 0 "#PWR0109" H 6550 3150 50  0001 C CNN
F 1 "GND" H 6555 3227 50  0000 C CNN
F 2 "" H 6550 3400 50  0001 C CNN
F 3 "" H 6550 3400 50  0001 C CNN
	1    6550 3400
	-1   0    0    1   
$EndComp
Text GLabel 7150 3700 2    50   Input ~ 0
V_in
Text GLabel 7150 3400 2    50   Input ~ 0
V_in
$Comp
L Device:R_US R1
U 1 1 5F4E0336
P 6700 3400
F 0 "R1" V 6495 3400 50  0000 C CNN
F 1 "0 Ohm" V 6586 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6740 3390 50  0001 C CNN
F 3 "~" H 6700 3400 50  0001 C CNN
	1    6700 3400
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R3
U 1 1 5F4E1630
P 7000 3400
F 0 "R3" V 6795 3400 50  0000 C CNN
F 1 "0 Ohm" V 6886 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7040 3390 50  0001 C CNN
F 3 "~" H 7000 3400 50  0001 C CNN
	1    7000 3400
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R2
U 1 1 5F4E42C9
P 6700 3700
F 0 "R2" V 6900 3700 50  0000 C CNN
F 1 "0 Ohm" V 6800 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6740 3690 50  0001 C CNN
F 3 "~" H 6700 3700 50  0001 C CNN
	1    6700 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R4
U 1 1 5F4E42CF
P 7000 3700
F 0 "R4" V 6795 3700 50  0000 C CNN
F 1 "0 Ohm" V 6886 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7040 3690 50  0001 C CNN
F 3 "~" H 7000 3700 50  0001 C CNN
	1    7000 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	6850 3700 6850 3600
Wire Wire Line
	6200 3600 6850 3600
Connection ~ 6850 3700
Wire Wire Line
	6850 3500 6850 3400
Wire Wire Line
	6200 3500 6850 3500
Connection ~ 6850 3400
$EndSCHEMATC
